/****************************************************************

 shrinkmaps.c

 =============================================================

 Copyright 1996-2022 Tom Barbalet. All rights reserved.

 Permission is hereby granted, free of charge, to any person
 obtaining a copy of this software and associated documentation
 files (the "Software"), to deal in the Software without
 restriction, including without limitation the rights to use,
 copy, modify, merge, publish, distribute, sublicense, and/or
 sell copies of the Software, and to permit persons to whom the
 Software is furnished to do so, subject to the following
 conditions:

 The above copyright notice and this permission notice shall be
 included in all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
 OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
 WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 OTHER DEALINGS IN THE SOFTWARE.

 This software is a continuing work of Tom Barbalet, begun on
 13 June 1996. No apes or cats were harmed in the writing of
 this software.

 ****************************************************************/

#include <stdio.h>
#include "pnglite.h"
#include "toolkit.h"

n_byte * read_png_file(n_string filename, png_t * ptr);

int write_png_file(n_string filename, int width, int height, n_byte *buffer);

n_string values[10]={ "/Users/barbalet/gitsdk/southeastengland1940maps/160.png",
                      "/Users/barbalet/gitsdk/southeastengland1940maps/161.png",
                      "/Users/barbalet/gitsdk/southeastengland1940maps/162.png",

                      "/Users/barbalet/gitsdk/southeastengland1940maps/170.png",
                      "/Users/barbalet/gitsdk/southeastengland1940maps/171.png",
                      "/Users/barbalet/gitsdk/southeastengland1940maps/172.png",
                      "/Users/barbalet/gitsdk/southeastengland1940maps/173.png",

                      "/Users/barbalet/gitsdk/southeastengland1940maps/182.png",
                      "/Users/barbalet/gitsdk/southeastengland1940maps/183.png",
                      "/Users/barbalet/gitsdk/southeastengland1940maps/184.png"};

n_string new_values[10]={ "/Users/barbalet/gitsdk/southeastengland1940maps/160_new.png",
                          "/Users/barbalet/gitsdk/southeastengland1940maps/161_new.png",
                          "/Users/barbalet/gitsdk/southeastengland1940maps/162_new.png",

                          "/Users/barbalet/gitsdk/southeastengland1940maps/170_new.png",
                          "/Users/barbalet/gitsdk/southeastengland1940maps/171_new.png",
                          "/Users/barbalet/gitsdk/southeastengland1940maps/172_new.png",
                          "/Users/barbalet/gitsdk/southeastengland1940maps/173_new.png",

                          "/Users/barbalet/gitsdk/southeastengland1940maps/182_new.png",
                          "/Users/barbalet/gitsdk/southeastengland1940maps/183_new.png",
                          "/Users/barbalet/gitsdk/southeastengland1940maps/184_new.png"};

#define XSIZE   11100
#define YSIZE   13500

#define XOFFSET 240
#define YOFFSET 300

#define XNEW   12000
#define YNEW   12000

n_int draw_error( n_constant_string error_text, n_constant_string location, n_int line_number )
{
    printf( "ERROR: %s @ %s %ld\n", ( const n_string ) error_text, location, line_number );
    return -1;
}

int main(int argc, n_constant_string argv[]) {
    // insert code here...
    printf("Hello, World!\n");
    return 0;
}
