# newlondmaps

Paid for Data from Library of Scotland.


## License
![cc logo](https://i.creativecommons.org/l/by/4.0/88x31.png )

All maps are licensed under a [Creative Commons Attribution 4.0 International License](http://creativecommons.org/licenses/by/4.0/).